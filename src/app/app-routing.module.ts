import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './v1/component/login/login.component';
import { WelcomeComponent } from './v1/component/welcome/welcome.component';
import { TodoListComponent } from './v1/todos/component/todo-list/todo-list.component';
import { ErrorComponent } from './v1/component/error/error.component';
import { LogoutComponent } from './v1/component/logout/logout.component';
import { RouteGuardService } from './v1/service/auth/route-guard.service';


const routes: Routes = [

  /**
   * Order of routes is important, and patterns are matched in order of appearance.
   * If the ** route is placed first, then all routes will be mapped to ** and be routed accordingly.
   */
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'welcome/:name', component: WelcomeComponent, canActivate: [RouteGuardService] },
  { path: 'list', component: TodoListComponent, canActivate: [RouteGuardService] },
  { path: 'logout', component: LogoutComponent, canActivate: [RouteGuardService] },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
