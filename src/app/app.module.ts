import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './v1/component/welcome/welcome.component';
import { LoginComponent } from './v1/component/login/login.component';
import { ErrorComponent } from './v1/component/error/error.component';
import { TodoListComponent } from './v1/todos/component/todo-list/todo-list.component';
import { MenuComponent } from './v1/component/menu/menu.component';
import { FooterComponent } from './v1/component/footer/footer.component';
import { LogoutComponent } from './v1/component/logout/logout.component';
import { HttpClientModule } from '@angular/common/http';
import { NotificationComponent } from './v1/notification/component/notification.component';
import { YesNoPipe } from './v1/pipe/yesno/yes-no.pipe';
import { TitleCasePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    ErrorComponent,
    TodoListComponent,
    MenuComponent,
    FooterComponent,
    LogoutComponent,
    NotificationComponent,
    YesNoPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    TitleCasePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
