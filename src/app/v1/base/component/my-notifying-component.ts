import { MyNotificationService } from '../../notification/service/notification.service';

export abstract class MyNotifyingComponent {

    constructor(protected notificationService: MyNotificationService) { }

    protected notifySuccess(messageText: string): void {
        this.notificationService.notifySuccess(messageText);
    }

    protected notifyError(messageText: string): void {
        this.notificationService.notifyError(messageText);
    }

    protected notifyWarning(messageText: string): void {
        this.notificationService.notifyWarning(messageText);
    }

    protected notifyInfo(messageText: string): void {
        this.notificationService.notifyInfo(messageText);
    }

    protected clearAllNotifications(): void {
        // clear messages
        this.notificationService.clearAllNotifications();
    }

}
