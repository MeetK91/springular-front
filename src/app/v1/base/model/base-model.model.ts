export abstract class BaseModel {

    public uuid: string;

    public createdAt: Date;

    public createdBy: string;

    public modifiedAt: Date;

    public modifiedBy: string;

}
