import { Component, OnInit, OnDestroy, destroyPlatform } from '@angular/core';
import { Router } from '@angular/router';
import { AuthFactory } from '../../service/auth/auth-factory';
import { MyNotifyingComponent } from '../../base/component/my-notifying-component';
import { MyNotificationService } from '../../notification/service/notification.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent extends MyNotifyingComponent implements OnInit, OnDestroy {

	userName = '';
	password = '';
	errorMessage = 'Wrong creds!!';
	invalidLogin = false;
	private authService: IAuthService;


	constructor(private router: Router, messagingService: MyNotificationService) {
		super(messagingService);
		this.authService = AuthFactory.create(AuthFactory.FIXED_AUTHENTICATION);
	}

	ngOnInit() {
		this.clearAllNotifications();
	}

	handleLogin() {
		this.invalidLogin = !this.authService.authenticate(this.userName, this.password);
		if (!this.invalidLogin) {
			// this.sendSuccessMessage("Login successful!");
			this.router.navigate(['welcome/' + this.userName]);
		} else {
			this.notifyError("Incorrect username or password! Please try again.");
		}
	}

	cleanUp(): void {
		this.authService = null;
		this.userName = null;
		this.password = null;
		this.errorMessage = null;
		this.invalidLogin = null;
	}

	ngOnDestroy(): void {
		this.cleanUp();
	}

}
