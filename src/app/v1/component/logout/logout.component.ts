import { Component, OnInit } from '@angular/core';
import { AuthFactory } from '../../service/auth/auth-factory';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  private authService: IAuthService;
  constructor() {
    this.authService = AuthFactory.create(AuthFactory.FIXED_AUTHENTICATION);
  }

  ngOnInit() {
    this.authService.logout();
  }

}
