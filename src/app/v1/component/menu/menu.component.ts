import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthFactory } from '../../service/auth/auth-factory';
// tslint:disable: indent
@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

	// fixedAuthService: FixedAuthenticationService;
	isUserLoggedIn: boolean;
	authService: IAuthService;

	constructor(private router: Router) {
		this.authService = AuthFactory.create(AuthFactory.FIXED_AUTHENTICATION);
	}

	ngOnInit() {
		this.isUserLoggedIn = this.authService.isUserLoggedIn();
	}

	logout() {
		this.router.navigate(['logout']);
	}

}
