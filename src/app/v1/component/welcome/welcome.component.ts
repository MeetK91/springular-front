import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WelcomeHttpService } from '../../service/http/welcome/welcome-http.service';
import { IHelloBean } from '../../todos/model/i-hello-bean';
import { MyNotifyingComponent } from '../../base/component/my-notifying-component';
import { MyNotificationService } from '../../notification/service/notification.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent extends MyNotifyingComponent implements OnInit {

  welcomeMessage = 'Hello ';
  successResponse: string;
  errorResponse: string;

  constructor(private route: ActivatedRoute, private welcomeHttpService: WelcomeHttpService, messagingService: MyNotificationService) {
    super(messagingService);
  }

  ngOnInit() {
    this.welcomeMessage += this.route.snapshot.params['name'];
    this.clearAllNotifications();
  }

  getWelcomeMessage(): void {
    this.welcomeHttpService.getWelcomeMessage()
      .subscribe(
        response => { this.handleSuccess(response); },
        error => { this.handleError(error); }
      );
  }

  handleSuccess(response: IHelloBean): void {
    this.notifySuccess(response.message);
  }

  handleError(error: any): void {
    this.notifyError(error.message);
  }

}
