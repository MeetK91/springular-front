import { Component, OnInit } from '@angular/core';
import { MyNotification } from '../model/my-notification';
import { MyNotifyingComponent } from '../../base/component/my-notifying-component';
import { MyNotificationService } from '../service/notification.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.css']
})
export class NotificationComponent extends MyNotifyingComponent implements OnInit {

	notifications: MyNotification[];
	hasData: boolean;
	private subscription: Subscription;

	constructor(notificationService: MyNotificationService) {
		super(notificationService);
	}

	ngOnInit() {
		this.notifications = [];
		this.hasData = false;

		this.subscription = this.notificationService.getMessage().subscribe(message => {
			if (message) {
				this.notifications.push(message);
				this.hasData = true;
			} else {
				// clear messages when empty message received
				this.notifications = [];
				this.hasData = false;

			}
		});
	}

	clearAll() {
		this.notifications = [];
		this.hasData = false;
	}

	removeNotification(notification: MyNotification) {
		const index: number = this.notifications.indexOf(notification);
		if (index !== -1) {
			this.notifications.splice(index, 1);
		}

		this.hasData = (this.notifications.length >= 1);
	}

}

