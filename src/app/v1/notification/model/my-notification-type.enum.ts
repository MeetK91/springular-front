export enum MyNotificationType {
    SUCCESS,
    INFO,
    WARN,
    ERROR
}