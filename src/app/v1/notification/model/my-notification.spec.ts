import { MyNotification } from './my-notification';
import { MyNotificationType } from './my-notification-type.enum';

describe('Message', () => {
  it('should create an instance', () => {
    expect(new MyNotification("Spec Info", MyNotificationType.INFO)).toBeTruthy();
  });
});
