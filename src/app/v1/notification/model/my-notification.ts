import { MyNotificationType } from './my-notification-type.enum';

export class MyNotification {

    constructor(public notificationText: string, public notificationType: MyNotificationType) {

    }

}