import { TestBed } from '@angular/core/testing';

import { MyNotificationService } from './notification.service';

describe('MyMessagingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyNotificationService = TestBed.get(MyNotificationService);
    expect(service).toBeTruthy();
  });
});
