import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MyNotification } from '../model/my-notification';
import { MyNotificationType } from '../model/my-notification-type.enum';

@Injectable({
	providedIn: 'root'
})
export class MyNotificationService {

	private subject = new Subject<MyNotification>();

	constructor() { }

	clearAllNotifications(): void {
		this.subject.next();
	}

	getMessage(): Observable<MyNotification> {
		return this.subject.asObservable();
	}

	notifySuccess(notificationText: string): void {
		this.subject.next(this.successNotification(notificationText));
	}

	notifyError(notificationText: string): void {
		this.subject.next(this.errorNotification(notificationText));
	}

	notifyWarning(notificationText: string): void {
		this.subject.next(this.warningNotification(notificationText));
	}

	notifyInfo(notificationText: string): void {
		this.subject.next(this.infoNotification(notificationText));
	}

	private successNotification(notificationText: string): MyNotification {
		return new MyNotification(notificationText, MyNotificationType.SUCCESS);
	}

	private errorNotification(notificationText: string): MyNotification {
		return new MyNotification(notificationText, MyNotificationType.ERROR);
	}

	private infoNotification(notificationText: string): MyNotification {
		return new MyNotification(notificationText, MyNotificationType.INFO);
	}

	private warningNotification(notificationText: string): MyNotification {
		return new MyNotification(notificationText, MyNotificationType.WARN);
	}

}
