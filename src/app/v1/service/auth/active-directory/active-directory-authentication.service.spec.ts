import { TestBed } from '@angular/core/testing';

import { ActiveDirectoryAuthenticationService } from './active-directory-authentication.service';

describe('ActiveDirectoryAuthenticationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActiveDirectoryAuthenticationService = TestBed.get(ActiveDirectoryAuthenticationService);
    expect(service).toBeTruthy();
  });
});
