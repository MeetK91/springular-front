import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ActiveDirectoryAuthenticationService implements IAuthService {

	constructor(private http?: HttpClient) {
	}

	authenticate(userName: string, password: string): boolean {
		throw new Error("Method not implemented.");
	}

	authenticateWithAD(userName: string, password: string, callback: string): boolean {

		const headers = new HttpHeaders(userName ? {
			authorization: 'Basic ' + btoa(userName + ':' + password)
		} : {});

		this.http.get('user', { headers }).subscribe(response => {
			if (response['name']) {
				return true;
			} else {
				return false;
			}
		});

		return false;

	}

	isUserLoggedIn(): boolean {
		throw new Error("Method not implemented.");
	}
	logout(): boolean {
		throw new Error("Method not implemented.");
	}
}
