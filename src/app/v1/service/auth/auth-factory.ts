import { Injectable } from '@angular/core';
import { FixedAuthenticationService } from './fixed/fixed-authentication.service';
import { ActiveDirectoryAuthenticationService } from './active-directory/active-directory-authentication.service';

@Injectable({
	providedIn: 'root'
})

export class AuthFactory {

	static readonly FIXED_AUTHENTICATION: number = 0;
	static readonly ACTIVE_DIRECTORY_AUTHENTICATION: number = 1;

	static create(type: number): IAuthService {

		switch (type) {
			case AuthFactory.FIXED_AUTHENTICATION:
				return new FixedAuthenticationService();
				break;

			case AuthFactory.ACTIVE_DIRECTORY_AUTHENTICATION:
				return new ActiveDirectoryAuthenticationService();
				break;

			default: return null;
		}

	}
}
