import { TestBed } from '@angular/core/testing';

import { FixedAuthenticationService } from './fixed-authentication.service';

describe('FixedAuthenticationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FixedAuthenticationService = TestBed.get(FixedAuthenticationService);
    expect(service).toBeTruthy();
  });
});
