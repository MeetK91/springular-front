import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class FixedAuthenticationService implements IAuthService {

	constructor() { }

	authenticate(userName: string, password: string): boolean {
		if (userName === 'admin' && password === 'admin') {
			sessionStorage.setItem('user', userName);
			return true;
		} else {
			return false;
		}
		return false;
	}

	isUserLoggedIn(): boolean {
		const user = sessionStorage.getItem('user');
		return !(user === null);
	}

	logout(): boolean {

		try {
			sessionStorage.removeItem('user');
			return true;
		} catch (e) {
			console.log(e);
			return false;
		} finally {
			console.log('Logout execution complete');
		}
	}
}
