// tslint:disable: indent
interface IAuthService {
	authenticate(userName: string, password: string): boolean;
	isUserLoggedIn(): boolean;
	logout(): boolean;
}
