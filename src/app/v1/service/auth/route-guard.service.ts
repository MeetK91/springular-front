import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthFactory } from './auth-factory';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {

  private authService: IAuthService;

  constructor(private router: Router) {
    this.authService = AuthFactory.create(AuthFactory.FIXED_AUTHENTICATION);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigate(['login']);
    };

    return this.authService.isUserLoggedIn();
  }

}
