import { Injectable, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { throwError, Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class HttpHelperService implements OnInit {

	private baseUrl: string;

	ngOnInit() {
	}

	constructor(private httpClient: HttpClient) {
		this.baseUrl = environment.api_base + environment.api_version;
		if (!this.isApiAvailable()) {
			throwError('Failed to connect with API!');
		}
	}

	isApiAvailable(): boolean {
		let resp = false;
		this.httpClient.get<boolean>(environment.api_base + "/").subscribe(response => resp = response);
		return resp;
	}

	post(uri: string, content: string) { }

	get<T>(uri: string): Observable<T> {
		return this.httpClient.get(this.baseUrl + uri) as Observable<T>;
	}

	put<T>(uri: string, content: string): Observable<T> {
		return this.httpClient.put<T>(uri, content) as Observable<T>;
	}

	delete<T>(uri: string): Observable<T> {
		return this.httpClient.delete<T>(this.baseUrl + uri) as Observable<T>;
	}

}
