import { TestBed } from '@angular/core/testing';

import { WelcomeHttpService } from './welcome-http.service';

describe('WelcomeHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelcomeHttpService = TestBed.get(WelcomeHttpService);
    expect(service).toBeTruthy();
  });
});
