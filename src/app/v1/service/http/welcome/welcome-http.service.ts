import { Injectable } from '@angular/core';
import { HttpHelperService } from '../http-helper.service';
import { IHelloBean } from 'src/app/v1/todos/model/i-hello-bean';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WelcomeHttpService {

  private uri: string = '/todos';
  pathVar: number;
  constructor(private http: HttpHelperService) { }

  public getWelcomeMessage(): Observable<IHelloBean> {
    this.pathVar = Math.round(Math.random() * 1000);
    return this.http.get<IHelloBean>(`${this.uri}/hello/${this.pathVar}`);
  }

}