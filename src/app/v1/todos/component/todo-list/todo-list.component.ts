import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Todo } from '../../model/todo.model';
import { TodoHttpService } from '../../service/todo-http.service';
import { MyNotifyingComponent } from 'src/app/v1/base/component/my-notifying-component';
import { MyNotificationService } from 'src/app/v1/notification/service/notification.service';
import { TitleCasePipe } from '@angular/common';

@Component({
	selector: 'app-list',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent extends MyNotifyingComponent implements OnInit {

	caption: string;
	columnHeaders: string[];
	rows: ListRow[];
	todos: Todo[];

	constructor(private todoService: TodoHttpService, messagingService: MyNotificationService, private titleCasePipe: TitleCasePipe) {
		super(messagingService);
	}

	ngOnInit() {

		if (!this.todos) {
			this.rows = [];
			this.todos = [];
			this.todoService.getStaticTodos().subscribe(response => { this.refreshList(response); });
		}

		this.caption = 'Todos';
		this.columnHeaders = ['#', 'Todo', 'Category', 'Priority', 'Due Date', 'Status'];
	}

	refreshList(newTodos: Todo[]) {
		let i = 0;

		this.generateTable(newTodos);

		newTodos.forEach(element => {
			++i;
			this.rows.push(new ListRow(i, "todos",
				element.uuid,
				[
					new ListColumnData(ListUtils.INTEGER_DATA_TYPE, "" + i),
					new ListColumnData(ListUtils.STRING_DATA_TYPE, element.todoText),
					new ListColumnData(ListUtils.STRING_DATA_TYPE, element.category.name),
					new ListColumnData(ListUtils.STRING_DATA_TYPE, element.priority.toString()),
					new ListColumnData(ListUtils.STRING_DATA_TYPE, "" + new Date(element.dueDate).toDateString()),
					new ListColumnData(ListUtils.BOOLEAN_DATA_TYPE, element.isCompleted ? "Completed" : "Pending")
				]))

		});
		this.todos = newTodos;
	}


	refreshData() {
		this.todoService.getStaticTodos().subscribe(response => { this.refreshList(response); });
	}

	view(uuid: string): void {
		this.notifyInfo(`Viewing Todo ${uuid}`);
	}

	edit(uuid: string): void {
		this.notifyWarning(`Editing Todo ${uuid}`);
	}

	delete(uuid: string): void {

		this.notifyError(`Deleting Todo ${uuid}`);

		// this.todoService.delete(uuid).subscribe(

		// );
	}

	generateTable(list: any[]) {

		var obj: any = JSON.stringify(list);
		var keys: string[] = Object.keys(list[0]);
		var values = [[], []];

		keys = keys.map((item) => {
			item = item.split(/(?=[A-Z])/).join(" ");
			return this.titleCasePipe.transform(item);
		});

		console.log(keys);
		var j = list.length;
		for (let i = 0; i < list.length; i++) {
			values[0][i] = Object.values(list[i]);
		}

		console.log(values);
		console.log("--DIRECT LOG--");
		console.log(Object.values(list[0]));
		var val1 = Object.values(list[0]);
		for (let i = 0; i < val1.length; i++) {
			console.log("instanceof TEST for \"" + val1[i] + "\": " + (val1[i] instanceof Object) + " -- " + (typeof val1[i]));
		}


	}

}

class ListRow {

	rowNumber: number;
	modelName: string;
	modelRowId: string;
	columnValues: ListColumnData[];

	constructor(rowNumber: number, modelName: string, modelId: string, columnValues: ListColumnData[]) {
		this.rowNumber = rowNumber;
		this.modelName = modelName;
		this.modelRowId = modelId;
		this.columnValues = columnValues;
	}
}

class ListColumnData {

	dataType: number;
	dataValue: string;

	constructor(dataType: number, dataValue: string) {
		this.dataValue = dataValue;
		this.dataType = dataType;
	}

}

export class ListUtils {

	static readonly UNDEFINED_DATA_TYPE = 0;
	static readonly INTEGER_DATA_TYPE = 1;
	static readonly STRING_DATA_TYPE = 2;
	static readonly DATE_DATA_TYPE = 3;
	static readonly BOOLEAN_DATA_TYPE = 4;
	static readonly BASE_URL = environment.api_base + environment.api_version + "/todos";
	static readonly VIEW_URI = ListUtils.BASE_URL + '/view/';
	static readonly EDIT_URI = ListUtils.BASE_URL + '/edit/';
	static readonly DELETE_URI = ListUtils.BASE_URL + '/delete/';

}