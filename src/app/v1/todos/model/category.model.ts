import { User } from 'src/app/v1/user/model/user.model';
import { BaseModel } from 'src/app/v1/base/model/base-model.model';

export class Category extends BaseModel {

    public name: string;

    public colorCode: string;

    public user: User;

}
