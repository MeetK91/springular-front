import { IHelloBean } from './i-hello-bean';

export class HelloBean implements IHelloBean {
    
    message: string;

    constructor(message: string) {
        this.message = message;
    }
    
    getMessage(): string {
        return this.message;
    }

    setMessage(message: string) {
        this.message = message;
    };

}