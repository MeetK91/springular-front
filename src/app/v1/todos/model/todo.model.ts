import { User } from 'src/app/v1/user/model/user.model';
import { BaseModel } from 'src/app/v1/base/model/base-model.model';
import { Category } from './category.model';

export class Todo extends BaseModel {

    public todoText: string;

    public dueDate: Date;

    public user: User;

    public isCompleted: boolean;

    public priority: number;

    public category: Category;

}
