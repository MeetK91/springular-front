import { Injectable } from '@angular/core';
import { HttpHelperService } from 'src/app/v1/service/http/http-helper.service';
import { Todo } from '../model/todo.model';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class TodoHttpService {

	constructor(private http: HttpHelperService) { }

	getStaticTodos(): Observable<Todo[]> {
		return this.http.get<Todo[]>("/todos/user/asd");

	}

	delete(uuid: string): Observable<void> {
		return this.http.delete(`/todos/delete/${uuid}`);
	}
}
