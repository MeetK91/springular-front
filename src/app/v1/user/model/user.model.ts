import { BaseModel } from 'src/app/v1/base/model/base-model.model';

export class User extends BaseModel {

    public userName: string;

    public passwordHash: string;

    public emailId: string;

    public recoveryEmailId: string;

    public isVerified: boolean;
}
