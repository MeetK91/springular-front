export const environment = {
  production: true,
  api_base: 'http://localhost:8080/springular',
  api_version: '/v1',
  app_title: 'Project Springular',
};
